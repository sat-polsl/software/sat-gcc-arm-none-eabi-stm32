![build](https://gitlab.com/sat-polsl/sat-gcc-arm-none-eabi-stm32/badges/master/pipeline.svg)

# Sat-gcc-arm-none-eabi-stm32

Dockerised Arch Linux based `gcc-arm-none-eabi` (10.3-2021.10) toolchain with CMake,
created for STM32 development.

Downloads and installs the newest version of `gcc-arm-none-eabi` toolchain
directly form
[ARM web page](https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm/downloads).
Additionally installs developer tools: desktop gcc for running test on a host
machine, python for running scripts and wget for downloading the toolchain.
